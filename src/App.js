import "./App.css";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import AppContainer from "./hoc/AppContainer";
import Login from "./components/Login/Login";
import Translation from "./components/Translation/Translation"
import NotFound from "./components/NotFound/NotFound";
import Profile from "./components/profile/Profile";
import logo from './logo/Logo.png'

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <AppContainer >
         <img width='50px' src={logo} ></img>
          <h4>Lost In Translation</h4>
        </AppContainer>
        <Switch>
          <Route path="/" exact component={Login} />
          <Route path="/translation" component={Translation} />
          <Route path="/profile" component={Profile} />
          <Route path="/" component={NotFound} />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
