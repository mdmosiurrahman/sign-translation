import {useHistory} from "react-router-dom"
import { useEffect, useState } from "react";
import signList from "../../utill/utility";

const Translation =()=> {


    // useHistory hook gives access to the history instance that we use to navigate
    const history = useHistory();

    //Declare a new state variable, which we call  translate character for user input
    const[translatechar, setTranslationChar] = useState("");

    //Declare a state variable which translated set of character
    let[letterToTranslate, setLetterToTranslate] = useState("");
    //letterToTranslate

    //After translation character pass to profile page
    const [translatedWordList, setTranslatedWordList] = useState([]);
        
    //access user name from session storage. 
    const userData = () => {
            let userInputData = sessionStorage.getItem("username"); 
            return userInputData
        }
     
    //its takes the input from user and set it to translate variable 
    const doTranslate = event => {
        setTranslationChar(event.target.value)
    }
    
    //After writing the letter and pressed enter the letter to  from english to sign-langauge.
    //This letter is pushed to the array for saving not more than 10 translation and finnaly dislpayed in Profile Page
    const handlePresEnter = (event) => {
        if(event.key === 'Enter') {
            if (translatedWordList.length<=10){
                setLetterToTranslate(letterToTranslate = translatechar.toLowerCase())
                setTranslatedWordList(translatedWordList.concat(translatechar.toLowerCase()))
                localStorage.setItem("translationsList", JSON.stringify(translatedWordList));
            }
            setTranslationChar(event.target.value = "")
                    
        }

        
    }
    
    const signs=signList()
    
    const goToHomePage = () => {
        history.push("/")
    }

    const goToProfile = () => {
        history.push("/profile")
        localStorage.setItem("translationsList", JSON.stringify(translatedWordList));
    }
    
    return(
        <div  >
            <div >
                <span onClick={goToHomePage} ></span>
            </div >
            <div onClick={goToProfile} className='profileName'> {userData()}</div>
                   
            <div className="inputChar">
                <input type="text" onChange={doTranslate} 
                 onKeyPress={handlePresEnter} value={translatechar} 
                 placeholder="Enter your letter for Translation"/>
            </div>

            <div >
                 <div >
                    <div >
                   
                    {letterToTranslate.split("").map(letter => (
                        (signs[`${letter}.png`] && <img width="50px" height="50px" 
                        src={signs[`${letter}.png`].default} />)
                    ))}
                    </div>
            </div>

            </div>
        </div>
    )
}

export default Translation