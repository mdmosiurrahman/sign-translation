export const LoginAPI = {

          login(credentials) {
          return fetch("http://localhost:8000/users",
      {
        method: "POST",
        headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(credentials),
      }).then(async (response) => {
      if (!response.ok) {
       //201.200 error
        const { error = "An unknown error occurred" } = await response.json();
        throw new Error(error); //force into catch
      } else{
        sessionStorage.setItem("username", credentials.username);
      }
      return response.json();
    });
  },
};
