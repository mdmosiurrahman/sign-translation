import AppContainer from "../../hoc/AppContainer";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loginAttemptAction } from "../../store/actions/loginActions";
import { Redirect } from "react-router";


const Login = () => {
  const dispatch = useDispatch();
  const { loginError, loginAttempting } = useSelector(state => state.loginReducer)
  const { loggedIn } = useSelector(state => state.sessionReducer)
  
  const [credentials, setCredentials] = useState({
    username: ""
  });

  const onInputChange = (event) => {
    setCredentials({
      ...credentials,
      [event.target.id]: event.target.value,
    });
  };
  const onFormSubmit = (event) => {
    event.preventDefault(); //stop the page relaod,We use hook to dispatch
    dispatch(loginAttemptAction(credentials)); //login
  };

  return (
      <>
      { loggedIn && <Redirect to="/translation" />}
      { !loggedIn &&
      <AppContainer>
      <form className="mt-3" onSubmit={onFormSubmit}>
        <h1>Lost in Translation</h1>
        <p>Get started 👑</p>
         
        <div className="mb-3">
          <label htmlFor="username" className="form-label" className='profileName'>
            Username
          </label>
          <input
            id="username"
            type="text"
            placeholder="Enter your name ?"
            className="form-control"
            onChange={onInputChange}
          />
        </div>
        
        <button type="submit" className="btn btn-primary btn-lg">
          Login
        </button>
       </form>
          {loginAttempting && <p>Trying to login..</p>}
          {
            loginError && 
            <div className="alert alert-danger" role="alert">
            <h4>Unsuccessful</h4>
            <p className="mb-0">{loginError}</p>
            </div> 
          }
    </AppContainer>
      }
      </>
                 
  )
}

export default Login;
