import signList from "../../utill/utility";

const ProfilePage = (poperties) =>{

    //declare variable to store all signs in an array
    const sign=signList()

    return(

        <div >
            <div >
                <p>{poperties.chars}</p>
            </div>
            {/*maps each character from word */}
                <div >
                        {poperties.chars.split("").map(char => (
                        (sign[`${char}.png`] && <img height="50px"width="50px" 
                        src={sign[`${char}.png`].default} />)
                    ))}
               </div>
        </div>

    )
}

export default ProfilePage