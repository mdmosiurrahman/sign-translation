import {Link,useHistory,useDispatch} from "react-router-dom";
import ProfilePage from "./ProfilePage";


 const  Profile = () =>{

    // useHistory hook gives us access to the history instance that we use to navigate
    const history = useHistory();
    //const dispatch = useDispatch

    // localStorage instance which has setter and getter to store and retrieve data from the local storage
    const getTranslatedDataFromStorege = () => {
        let translatedStoreData = JSON.parse(localStorage.getItem("translationsList"));
        return translatedStoreData
    }

    const getUserName = () => {
        let userdata = sessionStorage.getItem("username");
        return userdata
    }

    const backToPeiviousPage = () => {
        history.push("/")
       
    }

    const logOut =() =>{
          localStorage.clear() // Cleare local stirage
          history.push("/")
         
    }
    return(        
        <div>
            <span onClick={backToPeiviousPage} >
                Back
            </span>
                <div >
                    <Link to="/profile">
                    <div className='profileName'>
                        {getUserName()}
                    </div>
                    </Link>
                    <button type="submit" className="btn btn-primary btn-lg">
                        <span onClick={logOut}>Logout</span>
                        </button>
                </div>

                <div>
                 {
                 getTranslatedDataFromStorege()
                 .map(characterToPush=>(
                <ProfilePage chars={characterToPush}/>))
                } 
                
            
                </div>
        </div>

    )
}




export default Profile