
// we use this function to get all sign picture from sign folder. 
//After that we use signList() function in Translation page

const  signList =() =>{
    const  getSigns =(any)=> {

        let signimage = {};
        any.keys().map((image) => { signimage[image.replace('./', '')] = any(image); });
        return signimage;
    }

    const signimage = getSigns(require.context( './../signs' ,  false , /\.png/));
    return signimage
}

export default signList